
public class Main {

    public static void main(String[] args) {
        double area = calculateCircleArea(15.5);
        System.out.println(area);
    }

    // Your code here
    public static double calculateCircleArea(double radius){

        double area = Math.PI*(radius * 2);
        return area;
    }
}