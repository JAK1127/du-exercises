public class Main {

    public static void main(String[] args) {

        Flight lax1 = new Flight();
        Flight lax2 = new Flight();

        lax1.passengers = 100;
        lax2.passengers = 50;

        Flight lax3;

        if(lax1.hasRoom(lax2)) {
            lax3 = lax1.createNewWithBoth(lax2);
            System.out.println(lax3.passengers);
            lax3.add1Passenger();
        }

    }
}

 class Flight{
    public int passengers;
    public int seats;

   public Flight(){
        seats = 150;
        passengers = 0;
    }

    void add1Passenger(){
        if(passengers < seats){
            passengers += 1;
        }
        else
            handleTooMany();
    }

    void handleTooMany(){
        System.out.println("Too Many");
    }

    public boolean hasRoom(Flight f2){
       int total = passengers + f2.passengers;
       return total <= seats;
    }

    public Flight createNewWithBoth(Flight f2){
       Flight newFlight = new Flight();
       newFlight.seats = seats;
       newFlight.passengers = passengers + f2.passengers;
       return newFlight;
    }
}
