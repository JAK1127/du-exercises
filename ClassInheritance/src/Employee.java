public class Employee {

    private Integer idNumber;

    public Integer getIdNumber(){
        return idNumber;
    }

    public Employee(){

    }

    public Employee(Integer id){

        this.idNumber = id;
    }

    public boolean hasAdministratorRights(){
        return false;
    }

    @Override
    public boolean equals(Object o){

        Employee emp = (Employee) o;
        return idNumber == emp.idNumber;
    }


}
